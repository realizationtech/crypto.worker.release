#!/bin/bash
echo "run.sh started"
screen -d -m -S crypto.worker /opt/realization/crypto.worker.release/Crypto.Worker
update(){
        cd /opt/realization/crypto.worker.release
        git pull 1>&1 | grep "Already up-to-date."
        if [[ ! $? -eq 0 && ! $init ]]; then
                echo "Stopping crypto.worker.service"
                screen -X -S crypto.worker quit
                echo "Updating crypto.worker.release"
                git pull
                echo "Update complete"
                screen -d -m -S crypto.worker /opt/realization/crypto.worker.release/Crypto.Worker
                slackpost -t "$(hostaname) updated" -b "$(hostaname) has updated the worker" -c "crypto_alert" -u "https://hooks.slack.com/services/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"
        fi
}

while : # loop
do
        update
        echo "Sleeping 5 minutes"
        sleep 300
done